

"""
CuallixMC

provee herramientas para el proceso de clearing de Masterd Card
"""


import argparse
import glob

from FileUtil import *
from mciutil.mciutil import _convert_text_eb2asc
from CLXcommon import selectDirectory
from CLXLog import * 
from Validator import *



#Esto crea una instancia de log que identifica el modulo en que se esta ejecutando
log = CLXLog("CLX_MC")



def _main():
    
    selectMainDirectory()
    mainPath = getMainDirectory()
    filepath =  mainPath + "/recibidos"
    
    selectDirectory(filepath)

    for file in glob.glob("*"):
        
        nombre = file
        if nombre.split(".")[1] == "IPM" or nombre.split(".")[1] == "ipm": 
            
            salida = nombre.replace(".IPM", ".JSON") 
            impFile = IPMFile( filepath + "/" + nombre, salida, True)
        
            try:
                createDateDirectory(mainPath + "/enviados")
                if processFile(impFile):
                    moveToProcessed(filepath + "/" + nombre, mainPath)
                else:
                     raise Exception
            except Exception as ex:
                print("\nNo se pudo procesar el archivo: " + nombre)
                log.writeLogLine("No se pudo procesar el archivo: " + nombre, exception = ex,logLevel = Loglevel.ERROR)
       
        elif nombre.split(".")[1] == "JSON" or nombre.split(".")[1] == "json":
        
            try:
                createDateDirectory(mainPath + "/enviados")
                if convertToMessageFile(filepath + "/" + nombre):
                     moveToProcessed(filepath + "/" + nombre, mainPath)
                else:
                     raise Exception
            except Exception as ex:
                print("\nNo se pudo procesar el archivo: " + nombre)
                log.writeLogLine("No se pudo procesar el archivo: " + nombre, exception = ex,logLevel = Loglevel.ERROR)
    
    
           

    
if __name__ == "__main__":
    _main()

