
"""
Provee las herramientas necesarias para la validacion de PDS 
bajo los estandares establecidos por Mastercard

@Clases
    -RegexPattern
    -FormatValidator

@Dependencias
    -re
    -CLXlog
"""


import re

from aenum import Enum 
from CLXLog import *
from SQLBridge import *

#Creamos una instancia del log con el nombre del script actual
log = CLXLog(__name__)

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

sql = SQLBridge(os.path.join(__location__,"SQLite/CatalogoPDS.sqlite"))



class RegexPattern():

    """
        Clase auxiliar que contiene las expresiones regulares 
        para la validacion de los formatos usados por MasterCard

        @miembros
        A        - alfabetico
        N        - numerico
        AN       - alfanumerico
        AS       - alfabetico con espacios
        NS       - numerico con caracteres especiales
        ANS      - alfanumerico con caracteres especiales (incluidos espacios)
        DATE     - fecha en formato YYMMDD
        SHORDATE - fecha en formato YYMM

        @nota 
        Esta clase esta diseñada para ser utilizada similar a un Enum en C#
    """
    A         =  "^[A-z]+$"
    N         =  "^[0-9]+$"
    AS        =  "^[a-zA-Z ]+$"
    NS        =  "^[0-9 @~`!@#$%^&*()_=+\\\\';:\"\\/?>.<¿°|]+$"
    AN        =  "^[a-zA-Z0-9]+$"
    ANS       =  "^[0-9A-Za-z @~`!@#$%^&*()_=+\\\\';:\"\\/?>.<¿°|]+$"
    DATE      =  "^((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229)$"
    DATE_TIME =  "^((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229)((2[0-3]|[01]?0[0-9])([0-5]?[0-9])([0-5]?[0-9]))$" 
    SHORDATE  =  "^((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012]))))$"

class FormatError:
    """
        Representa un error de formato para un PDS y sus subcampos
    """

    PDS = ""
    Error = []

    def __init__(self, PDSId):
        self.PDS = "PDS" + str(PDSId).zfill(4)
       

    def addError(self, valor, descripcion):
        error = {}
        error['Valor'] = valor
        error['Descripcion'] = descripcion
        self.Error.append(error)

class FormatValidator:
    """
       Clase que provee herramientas para validar el formato de una cadena 
       ya sea usando Regex, comparando longitudes o comprobando su justificacion
    """
    
    def validateDE(self, DEId, string):
        """
        """

        if DEId < 0:
            return

        try:
            log.writeLogLine("Validando el  Data Element " + str(DEId), logLevel = Loglevel.INFO) 

            DErow = sql.get

        except Exception as Error:
            log.writeLogLine("Error al obtener el subcampo", exception = error,logLevel = Loglevel.ERROR)

    def validatePDS(self, PDSId, string):

        formatError = FormatError(PDSId)
        
        try:

            log.writeLogLine("Validando el  PDS " + str(PDSId), logLevel = Loglevel.INFO) 

            PDSrow = sql.getPDSRowList(PDSId)[0]

            if self.validateFormat(string, pattern = getPattern(PDSrow.TipoDatoId)):
                 log.writeLogLine("El PDS " + str(PDSId) + " tiene el formato correcto", logLevel = Loglevel.DEBUG) 
            else:
                 log.writeLogLine("El PDS " + str(PDSId) + " no tiene el formato correcto", logLevel = Loglevel.ERROR) 
                 formatError.addError(string, "El PDS " + str(PDSId) + " no tiene el formato correcto")

            if self.validateLength(string, PDSrow.LongitudMin, PDSrow.LongitudMax):
                 log.writeLogLine("El PDS " + str(PDSId) + " tiene la longitud correcta", logLevel = Loglevel.DEBUG) 
            else:
                 log.writeLogLine("El PDS " + str(PDSId).zfill(4) + " no tiene la longitud correcta, Longitud del dato: " + str(len(string)) + " Longitud esperada: " + str(PDSrow.LongitudMin) + " - " + str(PDSrow.LongitudMax), logLevel = Loglevel.ERROR) 
                 formatError.addError(string, "El PDS " + str(PDSId).zfill(4) + " no tiene la longitud correcta, Longitud del dato: " + str(len(string)) + " Longitud esperada: " + str(PDSrow.LongitudMin) + " - " + str(PDSrow.LongitudMax))

            SubcampoRows = sql.getSubCampoRowList(PDSId)
            stringPointer = 0

            for row in SubcampoRows:
               
                if row.LongitudMin != row.LongitudMax:
                    subfield = string[stringPointer:];

                    if len(subfield) < 1:
                        continue
                else:
                    subfield = string[stringPointer: stringPointer + row.LongitudMax]
                    
                log.writeLogLine("Valor del subcampo en posicion " + str(row.Posicion) + "- " + subfield, logLevel = Loglevel.WARNING) 

                if self.validateFormat(subfield, getPattern(row.TipoDatoId)):
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " tiene el formato correcto", logLevel = Loglevel.DEBUG) 
                else:
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " no tiene el formato correcto", logLevel = Loglevel.ERROR) 
                    formatError.addError(subfield, "El Subcampo en la posicion " + str(row.Posicion) + " no tiene el formato correcto")
                    
                if self.validateLength(subfield, row.LongitudMin, row.LongitudMax):
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " tiene la longitud correcta", logLevel = Loglevel.DEBUG) 
                else:
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " no tiene la longitud correcta, SubcampoId = " + str(row.SubcampoId), logLevel = Loglevel.ERROR) 
                    formatError.addError(subfield, "El Subcampo en la posicion " + str(row.Posicion) + " no tiene la longitud correcta, Longitud del dato: " + str(len(subfield)) + " Longitud esperada: " + str(row.LongitudMin) + " - " + str(row.LongitudMax))                    
                    
                if self.validateJustification(subfield, row.JustificacionId):             
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " tiene la justificacion correcta", logLevel = Loglevel.DEBUG) 
                else:                                                                     
                    log.writeLogLine("El Subcampo en la posicion " + str(row.Posicion) + " no tiene la justificacion correcta, SubcampoId = " + str(row.SubcampoId), logLevel = Loglevel.ERROR) 
                    formatError.addError(subfield, "El Subcampo en la posicion " + str(row.Posicion) + " no tiene la justificacion correcta, SubcampoId = " + str(row.SubcampoId))

                Catalogo = sql.getCatalogoRowList(row.SubcampoId)

                if len(Catalogo) > 0:
                    
                    if subfield not in Catalogo:
                        log.writeLogLine("El valor " + subfield +" no se encuentra en el catalogo de opciones, SubcampoId = " + str(row.SubcampoId), logLevel = Loglevel.ERROR)
                 
                stringPointer = stringPointer + len(subfield)


            if len(formatError.Error) > 0:
                return formatError
            else:
                return None;
        except Exception as error:
              log.writeLogLine("Error al obtener el subcampo", exception = error,logLevel = Loglevel.ERROR)
              return None

    def validateFormat(self, string, pattern = RegexPattern.A):
        """
            Valida el formato de la cadena por medio de expresiones regulares

            @param string
            Cadena a validar 

            @pattern
            Expresion regular extraida de la clase RegexPattern con la cual 
            se validara la cadena de entrada

            @return 
            True o False dependiendo si la cadena cumple con la expresion de entrada
        """
        aux = re.fullmatch(str(pattern),string)

        if aux == None:
            return False

        return (aux.string == string)

    def validateLength(self, string ,minLength, maxLength):
        """
            Valida la longitud minima y maxima de una cadena

            @param string
            Cadena a validar

            @param minLength
            Longitud minima con la que debe cumplir una cadena 
            debe ser >= 0

            @param maxLength
            Longitud maxima con la que debe cumplir una cadena 
            debe ser >= 0

            @return 
            True o False dependiendo si la cadena cumple con las longitudes dadas
        """
        if minLength < 0 or maxLength < 1:
            return False

        return (len(string) >= minLength and len(string) <= maxLength)

    def validateJustification(self, string, JustificationId):

        """
            Valida la justificacion de una cadena puede ser izquierda, derecha o ninguna

            @param string
            Cadena a validar

            @param JustificationId
            Id de la justificacion a validar, debe ser entre 0 - 2
                -0: ninguna
                -1: izquierda
                -2: derecha

            @return
            True o False dependiendo de si cumple con la justificacion dada
        """

        Length = len(string)

        if JustificationId == 0:
            return True

        if JustificationId == 1:
            """
                Validado Justificacion Izquierda, esta aplica
                solo para campos alfanumericos, la justificacion
                debe rellenarse con espacios
            """
            if string[0] != ' ' and string[-1] != ' ':
                return True

            i = 1
            count = 0

            while string[-i] == ' ':
                count = count + 1
                i = i + 1

                if count >= Length:
                    return False

            return (count > 0)
               

        if JustificationId == 2:
            """
                Validado Justificacion derecha, esta aplica
                solo para campos numericos o que representen 
                cantidades monetarias, la justificacion
                debe rellenarse con ceros
            """
            return True
            #TODO: encontrar un algoritmo que se adapte a la situacion

            if string[0] != 0:
                return True;

            i = 0
            count = 0

            while string[i] == '0':
                count = count + 1
                i = i + 1

                if count >= Length:
                    return False

            return (count > 0)

    def getPattern(regexNumber):

        if regexNumber == 1:
            return RegexPattern.A 

        if regexNumber == 2:
            return RegexPattern.N

        if regexNumber == 3:
            return RegexPattern.AS

        if regexNumber == 4:
            return RegexPattern.NS

        if regexNumber == 5:
            return RegexPattern.AN

        if regexNumber == 6:
            return RegexPattern.ANS

        if regexNumber == 8:
            return RegexPattern.DATE

        if regexNumber == 9:
            return RegexPattern.DATE_TIME

        if regexNumber == 10:
            return RegexPattern.SHORDATE