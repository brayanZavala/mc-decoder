from sqlalchemy import *
from CLXLog import *

#Creamos una instancia del log con el nombre del script actual
log = CLXLog(__name__)

class SQLBridge:
    """
        Representa una conexion con con una Base de datos SQLite
        que contiene informacion sobre los PDS y sus subcampos
    """
    DBPath = ""  #Ruta del archivo .sqlite
    engine = None  #Instancia de SQLite
    PDSTable = None #Instacia de la tabla tbl_PDS
    DETable = None #Instancia de la tabla tbl_CatalagoDE
    SubCamposTable = None #Instancia de la tabla tbl_Subcampo
    SubCamposDETable = None #Instancia de la tabla tbl_SubcampoDE
    CatalogoDETable = None #Instancia de la tabla tbl_CatalogoDE
    CatalogoTable = None #Instancia de la tbl_CatalogoSubcampo

    def __init__(self, DBPath):

        if len(DBPath) < 1:
            return

        connection = "sqlite:///" + DBPath

        log.writeLogLine("Trantando de conectar a la base de datos...",logLevel = Loglevel.INFO)
        log.writeLogLine("Ruta especificada: " + DBPath, logLevel = Loglevel.DEBUG)

        try:
           #creando el engine de base de datos
           engine = create_engine(connection)

           #Obteniendo las referencias de las tablas por metadata
           PDSMeta = MetaData(engine)
           self.PDSTable = Table("tbl_PDS",PDSMeta, autoload = true)
            
           SubCamposMeta = MetaData(engine)
           self.SubCamposTable = Table("tbl_Subcampo", SubCamposMeta, autoload = true)

           CatalogoMeta = MetaData(engine)
           self.CatalogoTable = Table('tbl_CatalogoSubcampos', CatalogoMeta, autoload = true)

           DEMeta = MetaData(engine)
           self.DETable = Table('tbl_DataElement', DEMeta, autoload = true)

           SubcamposDEMeta = MetaData(engine)
           self.SubCamposDETable = Table('tbl_SubcampoDE', SubcamposDEMeta, autoload = true)

           CatalogoDEMeta = MetaData(engine)
           self.CatalogoDETable = Table('tbl_CatalogoDE', CatalogoDEMeta, autoload = true)

        except Exception as error:
            log.writeLogLine("Error al conectar con SQLite", exception = error,logLevel = Loglevel.ERROR)
            return
        
        log.writeLogLine("Conexion establecida exitosamente",logLevel = Loglevel.INFO)

    #%----------------------------Obteniendo informacion de los PDS----------------------------%#

    def getPDSinfo(self, PDSid):
        """
            Ejecuta el equivalente a un select a la tabla tbl_PDS

            @param PDSid    
            Valor entero que representa la clave unica de un registro en tbl_PDS, debe ser >= 0
                            
            @return 
            Objeto con el total de filas retornadas del select

        """

        try:
            SelectQuery = self.PDSTable.select().where(self.PDSTable.c.PDSId == PDSid)

            result = SelectQuery.execute()

            return result

        except Exception as error:
             log.writeLogLine("Error al consultar los PDS", exception = error,logLevel = Loglevel.ERROR)

    def getSubCampos(self, PDSid):
        """
            Ejecuta el equivalente a un select a la tabla tbl_Subcampo

            @param PDSid    
            Valor entero que representa el PDS al cual un subcampo 
            esta relacionado en tbl_Subcampo, debe ser >= 0
                            
            @return 
            Objeto con el total de filas retornadas del select
        """

        try:
            SelectQuery = self.SubCamposTable.select().where(self.SubCamposTable.c.PDSId == PDSid)

            result = SelectQuery.execute()

            return result

        except Exception as error:
             log.writeLogLine("Error al consultar los Subcampos", exception = error,logLevel = Loglevel.ERROR)

    def getCatalogo(self, SubcampoId):
         """
           Ejecuta el equivalente a un select a la tabla tbl_CatalogoSubcampo

           @param SubcampoId    
           Valor entero que representa la clave unica de subcampo al cual un catalogo 
           esta relacionado en tbl_CatalogoSubcampo, debe ser >= 0
                           
           @return 
           Objeto con el total de filas retornadas del select
         """
         try:
             SelectQuery = self.CatalogoTable.select().where(self.CatalogoTable.c.SubcampoId == SubcampoId)

             result = SelectQuery.execute()

             return result
            
         except Exception as error:
             log.writeLogLine("Error al consultar los Subcampos", exception = error,logLevel = Loglevel.ERROR)

    def getPDSRowList(self, PDSId):
        """
          Consulta la informacion en tbl_PDS y la vacia en una lista de PDSrows

          @param PDSid    
          Valor entero que representa la clave unica de un registro en tbl_PDS, debe ser >= 0

          @return 
          Lista con PDSRows
        """
        PDSList = []

        SelectResult = self.getPDSinfo(PDSId)

        for row in SelectResult:
            PDSList.append(PDSRow(row))

        return PDSList

    def getSubCampoRowList(self, PDSId):

        """
          Consulta la informacion en tbl_Subcampo y la vacia en una lista de SubCampoRows

          @param PDSid    
          Valor entero que representa el PDS al cual un subcampo 
          esta relacionado en tbl_Subcampo, debe ser >= 0

          @return 
          Lista con SubCampoRows
        """

        List = []

        SelectResult = self.getSubCampos(PDSId)

        for row in SelectResult:
           List.append(SubCampoRow(row))

        return List

    def getCatalogoRowList(self, SubcampoId):
        """
          Consulta la informacion en tbl_CatalogoSubcampo y la vacia en una lista de strings

          @param SubcampoId    
          Valor entero que representa el Subcampo al cual un esta relacinado 
          esta relacionado en tbl_CatalogoSubcampo, debe ser >= 0

          @return 
          Lista de strings

          @nota
          Este metodo devuelve una lista de strings ya que de los catalogos solo necesitamos
          su valor para compararlo con los valores regresados por los archivos IPM
        """

        List = []

        SelectResult = self.getCatalogo(SubcampoId)

        for row in SelectResult:
           List.append(row['Valor'])

        return List

    #%----------------------------Obteniendo informacion de los Data Elements-------------------%#

    def getDataElementInfo(self, DEId):
        """
            Ejecuta el equivalente a un select a la tabla tbl_DataElement

            @param DEId    
            Valor entero que representa la clave unica de un registro en tbl_DataElement, debe ser >= 0
                            
            @return 
            Objeto con el total de filas retornadas del select

        """
        try:
            SelectQuery = self.PDSTable.select().where(self.DETable.c.DEId == DEId)

            result = SelectQuery.execute()

            return result

        except Exception as error:
             log.writeLogLine("Error al consultar los Data Elements", exception = error,logLevel = Loglevel.ERROR)

    def getSubcamposDE(self, DEId):
        """
            Ejecuta el equivalente a un select a la tabla tbl_SubcampoDE

            @param DEId    
            Valor entero que representa el DataElement al cual un subcampo 
            esta relacionado en tbl_SubcampoDE, debe ser >= 0
                            
            @return 
            Objeto con el total de filas retornadas del select
        """
        try:
            SelectQuery = self.SubCamposTable.select().where(self.SubCamposDETable.c.DEId == DEId)

            result = SelectQuery.execute()

            return result

        except Exception as error:
             log.writeLogLine("Error al consultar los Subcampos", exception = error,logLevel = Loglevel.ERROR)

    def getCatalogoDE(self, SubcampoId):
        """
           Ejecuta el equivalente a un select a la tabla tbl_CatalogoDE

           @param SubcampoId    
           Valor entero que representa la clave unica de subcampo al cual un catalogo 
           esta relacionado en tbl_CatalogoDE, debe ser >= 0
                           
           @return 
           Objeto con el total de filas retornadas del select
        """

        try:
             SelectQuery = self.CatalogoTable.select().where(self.CatalogoDETable.c.SubcampoId == SubcampoId)

             result = SelectQuery.execute()

             return result
            
        except Exception as error:
             log.writeLogLine("Error al consultar los Subcampos", exception = error,logLevel = Loglevel.ERROR)

    def getDataElementRowList(self, DEId):
        """
          Consulta la informacion en tbl_DataElement y la vacia en una lista de DataElementRow

          @param DEId    
          Valor entero que representa la clave unica de un registro en tbl_DataElement, debe ser >= 0

          @return 
          Lista con DataElementRow
        """
        DataElementList = []

        SelectResult = self.getDataElementInfo(DEId)

        for row in SelectResult:
            PDSList.append(DataElementRow(row))

        return DataElementList

    def getSubCampoDERowList(self, DEId):
        
        """
          Consulta la informacion en tbl_Subcampo y la vacia en una lista de SubCampoRows

          @param PDSid    
          Valor entero que representa el PDS al cual un subcampo 
          esta relacionado en tbl_Subcampo, debe ser >= 0

          @return 
          Lista con SubCampoRows
        """

        List = []

        SelectResult = self.getSubcamposDE(DEId)

        for row in SelectResult:
           List.append(SubCampoDERow(row))

        return List

    def getCatalogoDERowList(self, SubcampoId):
         """
            Consulta la informacion en tbl_CatalogoDE y la vacia en una lista de strings

            @param SubcampoId    
            Valor entero que representa el Subcampo al cual un esta relacinado 
            esta relacionado en tbl_CatalogoDE, debe ser >= 0

            @return 
            Lista de strings

            @nota
            Este metodo devuelve una lista de strings ya que de los catalogos solo necesitamos
            su valor para compararlo con los valores regresados por los archivos IPM
         """

         List = []

         SelectResult = self.getCatalogo(SubcampoId)

         for row in SelectResult:
           List.append(row['Valor'])

         return List

#%----------------------------Rows de PDS-------------------%#
class PDSRow:
    """
        Representa una fila de la tabla tbl_PDS

        @miembros
        -PDSId Int
        -LongitudMin Int
        -LongitudMax Int
        -NumSubcampos Int
        -JustificacionId Int
        -TipoDatoId Int
    """

    PDSId = 0
    LongitudMin = 0
    LongitudMax = 0
    NumSubcampos = 0
    JustificacionId = 0
    TipoDatoId = 0

    def __init__(self, args,):
        
        self.PDSId = args[0]
        self.LongitudMin = args[1]
        self.LongitudMax = args[2]
        self.NumSubcampos = args[3]
        self.JustificacionId = args[4]
        self.TipoDatoId = args[5]

class SubCampoRow:
    """
        Representa una fila de la tabla tbl_subcampos

        @miembros
        SubcampoId Int
        PDSId Int
        Posicion Int
        LongitudMin Int
        LongitudMax Int
        JustificacionId Int
        TipoDatoId Int

        @nota
        El campo esOpcional se omitio debido a que en la mayoria
        de los casos es inecesario por las reglas de negocio de Mastercard
    """

    SubcampoId = 0
    PDSId = 0
    Posicion = 0
    LongitudMin = 0
    LongitudMax = 0
    JustificacionId = 0
    TipoDatoId = 0
    
    def __init__(self, args):
        
        self.SubcampoId = args[0]
        self.PDSId = args[1]
        self.Posicion = args[2]
        self.LongitudMin = args[3]
        self.LongitudMax = args[4]
        self.JustificacionId = args[5]
        self.TipoDatoId = args[7]

class CatalogoRow:
    """
        Representa una fila de la tabla tbl_Catalogosubcampos

        @miembros
        SubcampoId Int
        Valor String
        Descripcion String
    """
    SubcampoId = 0
    Valor = ""
    Descripcion = ""

    def __init__(self, args):
        
        self.SubcampoId = args[0]
        self.Valor = args[1]
        self.Descripcion = args[2]

#%----------------------------Rows de Data Element-------------------%#
class DataElementRow:
    """
         Representa una fila de la tabla tbl_DataElement

         @miembros
        -DEId Int
        -LongitudMin Int
        -LongitudMax Int
        -NumSubcampos Int
        -JustificacionId Int
        -TipoDatoId Int
    """
    DEId = 0
    LongitudMin = 0
    LongitudMax = 0
    NumSubcampos = 0
    JustificacionId = 0
    TipoDatoId = 0

    def __init__(self, args,):
        
        self.DEId = args[0]
        self.LongitudMin = args[1]
        self.LongitudMax = args[2]
        self.NumSubcampos = args[3]
        self.JustificacionId = args[4]
        self.TipoDatoId = args[5]

class SubCampoDERow:
    """
        Representa una fila de la tabla tbl_subcamposDE

        @miembros
        SubcampoId Int
        DEId Int
        Posicion Int
        LongitudMin Int
        LongitudMax Int
        JustificacionId Int
        TipoDatoId Int
    """

    SubcampoId = 0
    DEId = 0
    Posicion = 0
    LongitudMin = 0
    LongitudMax = 0
    JustificacionId = 0
    TipoDatoId = 0

    def __init__(self, args):
        
        self.SubcampoId = args[0]
        self.DEId = args[1]
        self.Posicion = args[2]
        self.LongitudMin = args[3]
        self.LongitudMax = args[4]
        self.JustificacionId = args[5]
        self.TipoDatoId = args[7]

class CatalogoDERow:
    """
        Representa una fila de la tabla tbl_Catalogosubcampos

        @miembros
        SubcampoId Int
        Valor String
        Descripcion String
    """

    SubcampoId = 0
    Valor = ""
    Descripcion = ""

    def __init__(self, args):
        
        self.SubcampoId = args[0]
        self.Valor = args[1]
        self.Descripcion = args[2]