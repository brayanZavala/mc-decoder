
import logging
from logging.handlers import SocketHandler
import os
from os.path import expanduser, isdir 
from datetime import datetime




class Loglevel:
    DEBUG    = 1
    INFO     = 2
    WARNING  = 3
    ERROR    = 4
    CRITICAL = 5


class CLXLog:
    """
        Provee las utilidades necesarias para generar y manipular 
        archivos LOG que capturen la interaccion con MasterCard
    """
    LOGGER = None
    LOGGER2 = None 
    LogPath = ""

    def __init__(self, name):
        self.LogPath = self.selectLogPath() + "/CLXlog.txt"
        #filename=self.LogPath,
        logging.basicConfig( level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        self.LOGGER = logging.getLogger(name)
        self.LOGGER2 = logging.getLogger(name + "2")
        
        self.LOGGER.setLevel(1)
        self.LOGGER2.setLevel(1)
        
        socket_handler = SocketHandler('127.0.0.1', 19996)  # default listening address
        file_handler = logging.FileHandler(self.LogPath)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        
        self.LOGGER.addHandler(socket_handler)
        self.LOGGER2.addHandler(file_handler) 
       

    def selectLogPath(self):
        
        home = expanduser("~")
        os.chdir(home)

        selectLogDirectory("Archivos_CLX-MC")
        selectLogDirectory("Log")
        createLogDateDirectory(os.getcwd())
        return os.getcwd()

    def writeLogLine(self, message, exception = None, logLevel = Loglevel.DEBUG):
        
        if exception is None:
            self.setLevel(logLevel)(message)
        else:
            self.setLevel(logLevel)(message + " - Error: {0}".format(exception.args))
           

    def setLevel(self, logLevel = Loglevel.DEBUG):
        
        if logLevel == Loglevel.DEBUG:
            return lambda message: [self.LOGGER.debug(message), self.LOGGER2.debug(message)]
        
        if logLevel == Loglevel.INFO:
            return lambda message: [self.LOGGER.info(message), self.LOGGER2.info(message)]
        
        if logLevel == Loglevel.WARNING:
            return lambda message: [self.LOGGER.warn(message), self.LOGGER2.warn(message)]
        
        if logLevel == Loglevel.ERROR:
            return lambda message: [self.LOGGER.error(message), self.LOGGER2.error(message)]
        
        if logLevel == Loglevel.CRITICAL:
            return lambda message: [self.LOGGER.critical(message), self.LOGGER2.critical(message)]


def selectLogDirectory(path):
    
     if os.path.isdir(path):
         os.chdir(path)
     else:
         os.makedirs(path)
         os.chdir(path)
   

def createLogDateDirectory(path):
      
     mesAño = datetime.now().strftime("%Y-%m")
     dia = datetime.now().strftime("%d")
     
     selectLogDirectory(path)
     selectLogDirectory(mesAño)
     selectLogDirectory(dia)


