# -*- mode: python -*-

block_cipher = None


a = Analysis(['CLX_MC.py'],
             pathex=['C:\\Users\\brayanzavala\\source\\repos\\CLX-MC\\CLX-MC'],
             binaries=[],
             datas=[('C:\\Program Files (x86)\\Microsoft Visual Studio\\Shared\\Python36_64\\Lib\\site-packages\\mciutil\\cli\\mideu.yml','mciutil\\cli')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='CLX_MC',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='CLX_MC')
