
from mciutil.mciutil import _process_element
from CLXLog import * 
from Validator import * 

log = CLXLog(__name__)
validator = FormatValidator()

class DataElement:
        """
        Clase auxiliar para el manejo de DE,
        permite crear una estructura de datos ordenada 
        para su procesamientos

        DEName:  (String) Nombre del Data Element
        DENum:   (int) Posicion del Data Element
        DEValue: (String) Valor del Data Elemet
        """
        DEName = ""
        DENum = 0
        DEValue = ""

        def __init__(self, name, value):
            self.DEName = name
            self.DEValue = value
            self.extractDENum()
          
        def extractDENum(self):
            """
                Obtiene la posicion del DE a partir de su nombre (ej. DE17 = 17) 
                y la retorna en un entero
            """
            if self.DEName == "":
                 self.DENum = 0
                 return
        
            #Leemos la cadena desde el caracter 2 hasta el final
            stringNum = self.DEName[2:]
            
            #Nos aseguramos que el dato sea numerico
            if not stringNum.isnumeric():
                self.DENum = 0
                return

            #Convertimos el dato y lo retornamos
            self.DENum = int(stringNum)

class DEList:
        """
            Clase auxiliar que permite en agrupar listas 
            los datos de un mensage

            MessageInfo: (List) contiene datos relevantes del mensaje (MTI, BMP)
            Elements: (List) contiene los Data Elements que pudiese contener el mensaje
            PDS: (List) contiene los PSD o informacion adicional de los data elements
        """
        MessageInfo = []
        Elements = []
        PDS = []

        def assignDE(self, DataElement, config):
                """
                    Separa los Data Element y los organiza segun su tipo

                    PARAMETROS
                        DataElement: objeto que contiene la informacion del DE
                        config: diccionario con la configuracion estraida de mideu.yml
                """
                
                #Si no tiene nombre se retorna
                if DataElement.DEName == "":
                    return
                
                #Revisamos que el Data Element no sea un PDS
                if DataElement.DEName[:2] == "DE" and DataElement.DENum > 0:
                    
                    if config[DataElement.DENum]["field_type"] == "LLVAR":
                        #Si el DE es tipo LLVAR se pone la longitud antes de la
                        #cadena en formato "00"
                        lenght = len(DataElement.DEValue)
                        
                        if lenght < 10:
                              DataElement.DEValue = "0" + str(lenght) + DataElement.DEValue
                        else:
                              DataElement.DEValue = str(lenght) + DataElement.DEValue

                    elif config[DataElement.DENum]["field_type"] == "LLLVAR":
                         #Si el DE es tipo LLLVAR se pone la longitud antes de
                         #la cadena en formato "000"
                         lenght = len(DataElement.DEValue)

                         if lenght < 100 and lenght >= 10:
                              DataElement.DEValue = "0" + str(lenght) + DataElement.DEValue
                         elif lenght < 10:
                              DataElement.DEValue = "00" + str(lenght) + DataElement.DEValue
                         else:
                              DataElement.DEValue = str(lenght) + DataElement.DEValue

                    #Si es un DE de informacion adicional extraemos los PDS
                    if DataElement.DENum in [48, 62, 123, 124, 125]:
                        self.extractPDS(DataElement.DEValue.encode(), config)

                #Separando los DE en listas segun su numero
                if DataElement.DEName == "MTI" or DataElement.DEName == "BMP":

                    self.MessageInfo.append(DataElement)
                elif DataElement.DEName[:3] == "PDS":
                   
                    PDSId = int(DataElement.DEName[3:])

                    validator.validatePDS(PDSId, DataElement.DEValue)

                    if self.checkExistence(self.PDS, lambda DE: DE.DEName == DataElement.DEName):
                        
                        self.setElementValue(self.PDS, DataElement.DEValue ,lambda DE: DE.DEName == DataElement.DEName)

                    else:
                        self.PDS.append(DataElement)

                elif DataElement.DENum != 0:
                    self.Elements.append(DataElement)
               
        def processMessageFromJSON(self, Message, config):
                """
                    Retorna un arreglo de bytes con la informacion del mensaje 

                    PARAMETROS
                        Message: dict con los DE del mensaje
                        Config: archivo de configuracion mideu.yml
                    RETORNA: bytearray
                """
          
                #Toma todos los DE de un mensaje y los separa en listas

                for item in Message:
                        self.assignDE(DataElement(item, Message[item]), config)      

                #Ordenamos los DE segun su sequienciad EJ(DE1, DE2, DE3)
                self.Elements.sort(key = lambda x: x.DENum)

               

                self.packPDS()

                #Creamos y retornamos la cadena de bytes a partir de las listas
                return self.buildString()
                
        def buildString(self):
                """
                    Retorna una cadena de bytes con la informacion de un mensaje 
                    con el formato especificado en la ISO 8583
                    
                    PARAMETROS:  

                    RETORNA: bytearray
                """
                result = b''

                #Agregamos el MTI (Message Type Indicator) a la cadena
                result = result + self.getElement("MTI").encode()

               
                bytes = self.generateBitMap()
                    
                #convertimos los bits del bitmap en bytes con formato hexadecimal
                hexString = b''
                for byte in bytes:
                    hexString = hexString + self.toHex(byte)
           
                result = result + hexString

                #Agregamos los DE de forma ordenada
                for item in self.Elements:
                    result = result + item.DEValue.encode()
                
                return result
            
        def getElement(self, elementName):
            
            for element in self.MessageInfo:
                if element.DEName == elementName:
                    return element.DEValue

            return ""
       
        def getBitmapBytes(self):
            """
                Separa el bitmap en conjutos de 8 bits para poder ser procesado
                
                RETORNA: String[]
            """
            bytes = [self.getElement("BMP")[i:i + 8] for i in range(0, len(self.getElement("BMP")), 8)]
           
            return bytes

        def toHex(self, stringByte):
            """
                Convierte una dadena en formato "00000001" a un byte hexadecimal

                RETORNA: Byte
            """
            hexNum = 0

            if stringByte[0] == "1" :
                hexNum = hexNum | 128
           
            if stringByte[1] == "1":
                hexNum = hexNum | 64
            
            if stringByte[2] == "1":
                hexNum = hexNum | 32
            
            if stringByte[3] == "1":
                hexNum = hexNum | 16
            
            if stringByte[4] == "1":
                hexNum = hexNum | 8

            if stringByte[5] == "1":
                hexNum = hexNum | 4

            if stringByte[6] == "1":
                hexNum = hexNum | 2

            if stringByte[7] == "1":
                hexNum = hexNum | 1
           
            out = hexNum.to_bytes(1, 'big')

            return out

        def generateBitMap(self):

            bitmap = ""

            bitList = []

            for i in range(0,128):
                bitList.append("0")

            for DE in self.Elements:
                bitList[DE.DENum - 1] = "1"

            bitList[0] = "1"
            
            bitmap = ''.join(bitList)
            
            self.MessageInfo.append(DataElement("BMP", bitmap))
            
            return self.getBitmapBytes()
        
        def extractPDS(self, DEValue, config):
            """
                Toma el valor de cualquier DE de informacion adicional (48, 62, 123, 124, 125)
                y se para la informacion en los PDS correspondiente.

                PARAMETROS:
                    DEValue: (String) cadena del DE de informacion adicional
                
            """
            PDS = dict()

            try:
                PDS,length = _process_element(48,config[48],DEValue,'ascii')

                if len(PDS) > 0:
                    
                    for PDSTag, value in PDS.items():
                        if PDSTag[:2] == "DE":
                            continue
                        
                        DE = DataElement(PDSTag, value.decode())
                        self.PDS.append(DE)

            except Exception as error:
                log.writeLogLine("Error al separar PDS",error, Loglevel.ERROR)
        
        def packPDS(self):
            """
                Toma el valor de la lista de PDS, los concatena 
                en formato correcto y los acomoda en los DE de informacion adicional
            """         
            DELength = 0
            cont = 0
            List = [""]

            for PDS in self.PDS:
                result = PDS.DEName[3:]

                length = str(len(PDS.DEValue)).zfill(3)
                result = result + length
                result = result + PDS.DEValue

                if (DELength + len(result)) > 992:
                    List.append(result)
                    cont = cont + 1
                    DELength = len(result)
                else:
                    List[cont] = List[cont] + result
                    DELength = DELength + len(result)

            self.fillDEAditionalInfo(List)
                    
        def checkExistence(self,DEList, filter):
            """
                Revisa si un DE o PDS se encuentra en la lista
                PARAMETROS:
                    DElist: (DElist) lista que contiene los DE o PDS
                    filter: (Lambda) funcion que estable es filtro a seguir
                RETURN: true o false dependiendo si encuentra coincidencias
            """

            for x in DEList:
                if filter(x):
                    return True
            return False

        def setElementValue(self, DEList, value ,  filter):
            """
            """
            for x in DEList:
                if filter(x):
                    x.DEValue = value
                
        def fillDEAditionalInfo(self, list):
            """
                Toma una lista y llena los DE de informacion adicional 
                con los valores
                
                PARAMETRO: (List) lista con los valores
            """

            cont = 0
            for data in list: 
                
                data = str(len(data)).zfill(3) + data
                if cont == 0:
                    self.insertDE_PDS("DE48", data)

                if cont == 1:
                    self.insertDE_PDS("DE62", data)
                
                if cont == 2:
                    self.insertDE_PDS("DE123", data)
                
                if cont == 3:
                    self.insertDE_PDS("DE124", data)

                if cont == 4:
                    self.insertDE_PDS("DE125", data)

                cont = cont + 1

            
            if len(list) > 0:
                self.Elements.sort(key = lambda x: x.DENum)

        def insertDE_PDS(self, DEName, value):
            """
            """

            if self.checkExistence(self.Elements, lambda DE: DE.DEName == DEName):
                self.setElementValue(self.Elements, value, lambda DE: DE.DEName == DEName)
            else:
                self.Elements.append(DataElement(DEName, value))
                
        def clearData(self):
            """
                Limpia las listas de DE
            """
            self.Elements.clear()
            self.MessageInfo.clear()
            self.PDS.clear()


class BlankObj:
 def __repr__(self):
  return ""