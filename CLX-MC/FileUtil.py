import json
import yaml
import mciutil.mciutil

from DEutil import *
#importamos las funciones que nos ayudan a desbloquear y procesar archivos IPM
from mciutil.mciutil import unblock, vbs_unpack, get_message_elements,flip_message_encoding,  block,  vbs_pack, _convert_text_asc2eb
#importamos las funciones que nos ayudara a leer el archivo de configuracion mideu.yml
from mciutil.cli.common import get_config_filename, filter_data_list
#Importamos una utileria para copiar archivos
from shutil import copyfile

from CLXcommon import *

from CLXLog import * 

from Validator import * 

log = CLXLog(__name__)

class IPMFile:
    """
        Clase auxiliar para el manejo de archivos IPM
        provee una estructura para el paso de parametros
        a las funciones de procesamiento

        FileName:(String) es el nombre del archivo IPM de entrada (Ruta completa)  
        OutPutFile:(String) es nombre del archivo donde se escribiran los datos procesados (Ruta completa)
        1014Blocking:(Bool) indica si el archivo tiene un formato 1014 bloqueado
    """ 
    FileName = ""
    FileExtension = ""
    OutputFile = ""
    is1014Blocking = False

    """Crea un contructor que incializa los miembros del objeto"""
    def __init__(self, fileName, outputfile, blocking):
        self.FileName = fileName
        self.FileExtension = fileName.split(".")[1]
        self.OutputFile = outputfile
        self.is1014Blocking = blocking

    def printFile(self):
        print(self.FileName)
        print(self.OutPutFile)
        print(self.is1014Blocking)

def processFile(ipmFile):
    """
    Procesa un archivo IPM y extrae sus datos, puede escribir un archivo JSON de salida
    """
    log.writeLogLine("Iniciando procesamiento del archivo " + ipmFile.FileName.split("\\")[-1], logLevel = Loglevel.INFO)
    #Si el nombre de archivo viene vacio o la extension es diferente a IPM se sale
    if ipmFile.FileName == "" or ipmFile.FileExtension != "IPM":
        log.writeLogLine("Extencion no valida", logLevel = Loglevel.ERROR)
        return False

    try:
        #leemos el archivo de forma binaria (rb = lectura binaria)
        log.writeLogLine("Cargando el archivo...", logLevel = Loglevel.INFO)
        with open(ipmFile.FileName, 'rb') as infile:
            input_file = infile.read()
    except Exception as error:
        log.writeLogLine("Error al cargar el archivo " + ipmFile.FileName.split("\\")[-1],exception = error, logLevel = Loglevel.ERROR)
        return False


    #revisamos si el archivo tiene bloqueo 1014
    if ipmFile.is1014Blocking:
        input_file = unblock(input_file)#si esta bloqueado invocamos unblock de mciutil
    else:
        input_file = vbs_unpack(input_file)#si no esta bloqueado invocamos vbs_unpack de mciutil

    log.writeLogLine("Archivo cargado correctamente", logLevel = Loglevel.INFO)
    
    try:
        #obtenemos el archivo de configuracion mideu.yml
        log.writeLogLine("Obteniendo ruta del archivo de configuracion...", logLevel = Loglevel.DEBUG)
        config_fileName = get_config_filename("mideu.yml")
        log.writeLogLine("Ruta del archivo de configuracion: " + config_fileName, logLevel = Loglevel.DEBUG)
    
    except Exception as error:
         log.writeLogLine("Error al obtener la ruta del archivo de configuracion",exception = error, logLevel = Loglevel.ERROR)
         return False

    try:
        #obtenemos la configuracion yaml
        log.writeLogLine("Cargando archivo de configuracion...", logLevel = Loglevel.INFO)
        with open(config_fileName, "r") as config_file:
            config = yaml.load(config_file)
    except Exception as error:
        log.writeLogLine("Error al cargar el archivo de configuracion",exception = error, logLevel = Loglevel.ERROR)
        return False

    try:
        #obtenemos los mensajes del IPM invocando get_message_elements de mciutil
        log.writeLogLine("Procesando los registros del archivo...\n\n", logLevel = Loglevel.INFO)

        output_list = []
        cont = 0

        for record in input_file:
            log.writeLogLine("\n*************************************************************************", logLevel = Loglevel.INFO)
            log.writeLogLine("Procesando registro " + str(cont), logLevel = Loglevel.INFO)
            output_list.append(get_message_elements(record, config["bit_config"], "ebcdic"))
          
            cont = cont + 1
           

        log.writeLogLine("Registros procesados: " + str(len(output_list)), logLevel = Loglevel.INFO)

    except Exception as error:
         log.writeLogLine("Error al procesar los registros", exception = error, logLevel = Loglevel.ERROR)
         return False


    mkJSONDir()

    try:
        #obtenemos una archivo JSON que contendra solo los data elements con valor
        log.writeLogLine("Convirtiendo registros a JSON...", logLevel = Loglevel.INFO)
        IPM_to_JSON(output_list, config['output_data_elements'], ipmFile.OutputFile)
        log.writeLogLine("Archivo JSON generado exitosamente ruta: " + ipmFile.OutputFile, logLevel = Loglevel.INFO)
    except Exception as error:
        log.writeLogLine("Error generando el archivo JSON", exception = error, logLevel = Loglevel.ERROR)
        return False

    print("\nSe creo el archivo: {0}".format(ipmFile.OutputFile))
    print("\n\nProceso completo\nMensajes procesados: {0}".format(len(input_file)))
    return True
   
def IPM_to_JSON(data, fields, outputFileName):

    """
        Genera un archivo JSON apartir de los mensajes de un archivo IPM
    """
    #Filtramos los datos para obtener solo los llenos
    filtered_data_list = filter_data_list(data, fields)
    
    validateList(filtered_data_list)

    #Creamos un archivo en modo de escritura
    jsonFile = open(outputFileName,"w")
    
    #Transformamos la informacion al formaro JSON
    data = json.dumps(filtered_data_list)

    #Escribimos la informacion en el archivo
    jsonFile.write(data)
       
    #Cerramos el archivo 
    jsonFile.close()

def loadJSON(fileName):
    """
        Lee un archivo JSON y vacia el contenido en un diccionario
        PARAMETROS
            fileName: ruta y nombre del archivo JSON
        RETORNA: dict
    """
    data = json.load(open(fileName))
    return data

def convertToMessageFile(fileName):
    """
        Procesa un objeto JSON y crea un archivo con los datos de los mensajes en
        encoding EBCDIC
        PARAMETROS
            fileName: nombre del archivo JSON que contiene la informacion
        RETORNA: String
    """
    
    #Cargamos el JSON y el archivo de configuracion
    jsonFile = loadJSON(fileName)
    config_filename = get_config_filename("mideu.yml")

    with open(config_filename, 'r') as config_file:
       config = yaml.load(config_file)

    #Procesamos todos los mensajes del JSON y los descomponemos en listas
    result = []

    try:
        for Message in jsonFile:
            DElist = DEList()
            result.append(DElist.processMessageFromJSON(Message, config["bit_config"]))
            DElist.clearData()
    except Exception as error: 
         log.writeLogLine("Error separando los Data Elements, revise el formato del JSON", exception = error, logLevel = Loglevel.ERROR)
         return False

    output_records = []
    cont = 0
    #Convertimos los mensajes a EBCDIC
    try:

        for record in result:
            log.writeLogLine("\n*************************************************************************", logLevel = Loglevel.INFO)
            log.writeLogLine("Procesando el mensaje {0} del archivo JSON".format(cont), logLevel = Loglevel.INFO)
            output_records.append(flip_message_encoding( record, config["bit_config"], 'ascii'))
            cont = cont + 1
  
    except Exception as error: 
         log.writeLogLine("Error convirtiendo los registro a EBCDIC", exception = error, logLevel = Loglevel.ERROR)
         return False
    #Empaquetamos los mensajes 
    #output_data = vbs_pack(output_records)
    output_data = block(output_records)
    
    mkIPMDir()
    file = fileName.split("/")[-1]
    
    newFile = os.getcwd()
    tempFile = fileName.split("/")[:-2]
        
    #Escribimos la info en un archivo .IPM
    with open(file.split(".")[0] + ".IPM", "wb") as out:
          out.write(output_data)

    tempString = "/".join(tempFile) + "/Temp" 
    selectDirectory(tempString)

    copyfile(newFile + "/" + file.split(".")[0] + ".IPM", tempString + "/" + file.split(".")[0] + ".IPM")
    return True

def validateList(DEList):
    """
        Recorre una lista que contiene la informacion de lo DE
        y valida los PDS contenidos en ellos

        @param DElist 
        lista de DE

        @return ????
    """

    if len(DEList) <  1:
        return

    validator = FormatValidator()

    for item in DEList:
        print(item)
    


