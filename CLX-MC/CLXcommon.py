

import os.path
from datetime import datetime
from CLXLog import *


log = CLXLog(__name__)

def mkIPMDir():
     selectDirectory("IPM")
    
def mkJSONDir():
     selectDirectory("JSON")

def selectDirectory(path):
    
    try:
        log.writeLogLine("Seleccionando directorio " + path, logLevel =  Loglevel.INFO)
        if os.path.isdir(path):
            os.chdir(path)
        else:
            os.makedirs(path)
            os.chdir(path)
    except Exception as error:
        log.writeLogLine("Error al seleccionar directorio", error, Loglevel.ERROR)

def createDateDirectory(path):
    
    log.writeLogLine("Creando directorio por fechas", logLevel =  Loglevel.INFO)
    mesAño = datetime.now().strftime("%Y-%m")
    dia = datetime.now().strftime("%d")
    
    selectDirectory(path)
    selectDirectory(mesAño)
    selectDirectory(dia)
    
def moveToProcessed(fileName, mainPath):

    
    proccessedPath = mainPath + "/procesados" 
    log.writeLogLine("Moviendo el archivo " + fileName + " a directorio de procesados", logLevel =  Loglevel.INFO)
    selectDirectory(proccessedPath)

    createDateDirectory(proccessedPath)

    dateDir = os.getcwd()

    selectDirectory(dateDir + "/IPM")
    selectDirectory(dateDir + "/JSON")

    try:
        if fileName.split(".")[-1] == "JSON" or fileName.split(".")[-1] == "json":
            deleteFileIfExist(dateDir + "/JSON/" + fileName.split("/")[-1])
            os.rename(fileName, dateDir + "/JSON/" + fileName.split("/")[-1])
        elif fileName.split(".")[-1] == "IPM" or fileName.split(".")[-1] == "ipm":
            deleteFileIfExist(dateDir + "/IPM/" + fileName.split("/")[-1])
            os.rename(fileName, dateDir + "/IPM/" + fileName.split("/")[-1])
        log.writeLogLine("Archivo movido correctamente", logLevel =  Loglevel.INFO)
    except Exception as error:
        log.writeLogLine("Error al mover el archivo " + fileName + " a la carpeta procesados", error, Loglevel.ERROR)

def deleteFileIfExist(filePath):

    try:
        if os.path.isfile(filePath):
            nombre = filePath.split("/")[-1]
            log.writeLogLine("El archivo "+ nombre + "ya existe en el directorio, eliminando la version anterior... ", logLevel = Loglevel.WARNING)
            os.remove(filePath)
            log.writeLogLine("archivo eliminado", logLevel = Loglevel.WARNING)
    except Exception as error:
            log.writeLogLine("Error al eliminar el archivo " + filePath, error, Loglevel.ERROR)

def selectMainDirectory():

    home = expanduser("~")
    os.chdir(home)

    selectDirectory("Archivos_CLX-MC")
    selectDirectory("enviados")
    selectDirectory(home + "\\Archivos_CLX-MC")
    selectDirectory("procesados")
    selectDirectory(home + "\\Archivos_CLX-MC")
    selectDirectory("recibidos")
    selectDirectory(home + "\\Archivos_CLX-MC")
    selectDirectory("Temp")
    selectDirectory(home + "\\Archivos_CLX-MC")
    selectDirectory("Log")

    selectDirectory(home + "\\Archivos_CLX-MC")

def getMainDirectory():
     home = expanduser("~")
     os.chdir(home)
     selectDirectory(home + "\\Archivos_CLX-MC")
     return os.getcwd() 