## PROCESADOR DE ARCHIVOS IPM

### DESCRIPCION:
Aplicacion python que lee y procesa archivos de formato IPM (MasterCard) a JSON y viceversa, los archivos IPM vienen con un encoding EBCDIC por lo que es
necesario convertirlos a ASCII para despues descomponer su formato (ISO 8586) a un formato JSON facilmente tratable por cualquier lenguaje

